<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\facades\Auth;

class AuthController extends Controller
{
    public function register(Request $request)
    {
    	$validatedData	= 	$request->validate([
    		'name'		=>	'required|string|max:200',
    		'email'		=>	'required|string|email|max:200|unique:users',
    		'password'	=>	'required|string|min:8',
    	]);
    	$user 	= 	User::create([
    		'name'	=>	$validatedData['name'],
    		'email'	=>	$validatedData['email'],
    		'password'	=> Hash::make($validatedData['password']),
    	]);

    	$token	=	$user->createToken('auth_token')->plainTextToken;

    	return response()->json([
    		'access_token'	=>	$token,
    		'token_type'	=>	'Bearer',
    	]);
    }

    public function login(Request $request)
    {
    	if (!Auth::attempt($request->only('email', 'password')))
    	{
    		return response()->json([
    			'message'	=> 'Invalid login details'
    		], 401);
    	}

    	$user 	= User::where('email', $request['email'])->firstOrfail();
    	$token  = $user->createToken('auth_token')->plainTextToken;

    	return response()->json([
    		'access_token'	=>	$token,
    		'token_type'	=>	'Bearer',
    	]);

    }

    public function me(Request $request)
    {
    	return $request->user();
    }
}
