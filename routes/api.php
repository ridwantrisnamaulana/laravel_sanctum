<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// api me
Route::get('/me', [AuthController::class, 'me'])->middleware('auth:sanctum');
// api register
Route::post('/register', [AuthController::class, 'register']);
// api login
Route::post('/login', [AuthController::class, 'login']);
